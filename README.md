This repository contains a minimal example of how to setup a pipeline in GitLab CI.
You can go through the commit history
(see [here](https://gitlab.com/sustainable-simulation-software/minimal-ci-example/-/commits/main/)
to check how certain aspects of a pipeline can be implemented.

This example accompanies the material of the course on sustainable simulation software
development hosted at 
[gitlab.com/sustainable-simulation-software/course-material](https://gitlab.com/sustainable-simulation-software/course-material).
